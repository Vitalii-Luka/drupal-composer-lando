# Lando - based Drupal development infrastructure.

## Local infrastructure ##

Local development infrastructure consists of:

```
- PHP 7.4
- Nginx
- MySQL
- phpMyAdmin
- Mailhog
```

## Usage

First you need to install [Docker](https://docs.docker.com/engine/install/) and [Lando](https://docs.lando.dev/basics/installation.html#system-requirements)


After that you can create the project:

```
- $ cd your-project-directory
- $ git clone this-repository
- edit file hosts in the directory /etc
- $ sudo nano hosts
      127.0.0.1 drupal.example.local
      127.0.0.1 phpmyadmin.drupal.local
      127.0.0.1 mail.drupal.local
- $ cd drupal-composer-lando
- $ lando composer install
- $ lando start
```
[Lando Default Commands](https://docs.lando.dev/basics/usage.html#default-commands)


## What does the template do?

When installing the given `composer.json` some tasks are taken care of:

* Drupal will be installed in the `web`-directory.
* Autoloader is implemented to use the generated composer autoloader in `vendor/autoload.php`,
  instead of the one provided by Drupal (`web/vendor/autoload.php`).
* Modules (packages of type `drupal-module`) will be placed in `web/modules/contrib/`
* Theme (packages of type `drupal-theme`) will be placed in `web/themes/contrib/`
* Profiles (packages of type `drupal-profile`) will be placed in `web/profiles/contrib/`
* Creates default writable versions of `settings.php` and `services.yml`.
* Creates `web/sites/default/files`-directory.
* Latest version of drush is installed locally for use at `vendor/bin/drush`.
* Latest version of DrupalConsole is installed locally for use at `vendor/bin/drupal`.
* Creates environment variables based on your .env file. See [.env.example](.env.example).

